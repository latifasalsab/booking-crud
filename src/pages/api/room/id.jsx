import nextConnect from "next-connect";
import { getRoomById } from "controller/room";

const handler = nextConnect();
handler.post(getRoomById);

export default handler;
