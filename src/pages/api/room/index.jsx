import nextConnect from "next-connect";
import {
  getAllRoom,
  createRoom,
  updateRoom,
  deleteRoom,
} from "controller/room";

const handler = nextConnect();
handler.get(getAllRoom);
handler.post(createRoom);
handler.put(updateRoom);
handler.delete(deleteRoom);

export default handler;
