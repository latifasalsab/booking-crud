import { executeQuery } from "../config/db";

const getAllRoom = async (req, res) => {
  try {
    let data = await executeQuery("SELECT * from room", []);
    res.send(data);
  } catch (err) {
    res.status(500).json(err);
  }
};

const getRoomById = async (req, res) => {
  let id = req.body.id;
  try {
    let data = await executeQuery(`SELECT * from room WHERE id=${id}`, []);
    res.send(data);
  } catch (err) {
    res.status(500).json(err);
  }
};

const createRoom = async (req, res) => {
  const name = req.body.name;
  const nim = req.body.nim;
  const date = req.body.date;
  const time = req.body.time;
  const agenda = req.body.agenda;

  try {
    await executeQuery(
      `INSERT INTO room (name, nim, date, time, agenda) VALUES (?,?,?,?,?)`,
      [name, nim, date, time, agenda]
    );
  } catch (err) {
    res.status(500).json(err);
  }
};

const updateRoom = async (req, res) => {
  const id = req.body.id;
  const name = req.body.name;
  const nim = req.body.nim;
  const date = req.body.date;
  const time = req.body.time;
  const agenda = req.body.agenda;

  try {
    await executeQuery(
      `UPDATE room SET name = "${name}", nim = "${nim}", date = "${date}", time = "${time}", agenda = "${agenda}" WHERE id=${id}`
    );
  } catch (err) {
    res.status(500).json(err);
  }
};

const deleteRoom = async (req, res) => {
  const id = req.body.id;

  try {
    await executeQuery(`DELETE FROM room WHERE id=${id};`);
  } catch (err) {
    res.status(500).json(err);
  }
};

export { getAllRoom, getRoomById, createRoom, updateRoom, deleteRoom };
